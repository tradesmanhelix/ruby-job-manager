#!/usr/bin/ruby -w

require File.join(File.dirname(__FILE__), "job_manager")

job_manager = JobManager.new

while true
  jobs = ""

  loop do
    print "Job: "
    job = gets.chomp

    print "Dependency (optional): "
    dep = gets.chomp

    jobs += "#{job} => #{dep}\n"

    print "Add another (y/N)? "
    continue = gets.chomp

    break if continue != "y"
  end

  ordered_jobs = job_manager.order_jobs(jobs)
  puts "Job order is: #{ordered_jobs}\n"

  print "Continue? (y/N)? "
  break if gets.chomp != "y"
end
