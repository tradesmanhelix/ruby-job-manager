require "tsort"

# Class for resolving dependencies that can
# be represented as a directed graph.
#
# Adapted from:
# - http://ruby-doc.org/stdlib-2.5.0/libdoc/tsort/rdoc/TSort.html
# - https://www.viget.com/articles/dependency-sorting-in-ruby-with-tsort/
# - https://github.com/alpinstang/ruby-code-test
class DependencyResolver < Hash
  include TSort

  alias tsort_each_node each_key
  def tsort_each_child(node, &block)
    fetch(node, []).each(&block)
  end

  def resolve_dependencies
    self.tsort
  end 
end
