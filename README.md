# Ruby Job Manager
(By Alex Smith @ tradesmanhelix.com)

Job manager in Ruby. Handles the execution order of jobs and ensures there are no invalid dependencies between jobs.

Under the hood, relies on the excellent TSort class from the Ruby standard library to do the heavy lifting.

Inspired by: https://github.com/alpinstang/ruby-code-test.

# Usage
The tests can be run using the following command:

    $ ruby job_manager_test.rb

Additionally, an interactive prompt can be accessed via:

    $ ruby main.rb
