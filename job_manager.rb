require File.join(File.dirname(__FILE__), "dependency_resolver")

# JobManager
# Class for managing and validating jobs
class JobManager

  def initialize
    @dependency_resolver = DependencyResolver.new
  end

  # Driver method for ordering of jobs
  def order_jobs(jobs)
    ordered_jobs = ""

    if jobs
      begin
        parse_and_queue_jobs(jobs)
        resolved_jobs = @dependency_resolver.resolve_dependencies
        ordered_jobs = resolved_jobs.join
      rescue TSort::Cyclic => cycle
        puts "Error: Circular dependencies detected. #{cycle.message}"
      rescue StandardError => err
        puts err.message
      end
    end

    return ordered_jobs
  end
  
  private

  # Parses jobs via newlines and the rocket operator
  # and places them into the DependencyResolver hash
  def parse_and_queue_jobs(jobs)
    existing_jobs = Array.new
    required_jobs = Array.new

    # Using a variable here for easier debugging
    job_arr = jobs.split("\n")
    job_arr.each do |job_data|
      job, dep = job_data.split("=>")

      # Remove any trailing whitespace from jobs and dependencies
      job.strip!
      dep = (dep.instance_of? String) ? dep.strip! : ""

      # Keep track of which jobs are depended upon and which exist
      existing_jobs.push(job)

      if dep != ""
        required_jobs.push(dep)
      end

      if verify_valid_job(job, dep)
        @dependency_resolver[job] ||= []
        @dependency_resolver[job].push(dep)
      end
    end

    required_jobs.each do |req_job|
      if !existing_jobs.include? req_job
        raise "Error: Job #{req_job} is required but does not exist."        
      end
    end
  end

  def verify_valid_job(job, dep)
    if job == dep
      raise "Error: Job #{job} depends on itself."
    else
      true
    end
  end

end
