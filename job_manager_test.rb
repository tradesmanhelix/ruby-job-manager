require File.join(File.dirname(__FILE__), "job_manager")
require "test/unit"

class TestJobManager < Test::Unit::TestCase

  def setup
    @job_manager = JobManager.new
  end

  def test_nil_input
    input = nil
    ordered_jobs = @job_manager.order_jobs(input)

    assert_equal ordered_jobs, "", "input = #{input}, result = #{ordered_jobs}"
  end

  def test_blank_input
    input = ""
    ordered_jobs = @job_manager.order_jobs(input)

    assert_equal ordered_jobs, "", "input = #{input}, result = #{ordered_jobs}"
  end

  def test_single_valid_job
    input = "a =>"
    ordered_jobs = @job_manager.order_jobs(input)

    assert_equal ordered_jobs, "a", "input = #{input}, result = #{ordered_jobs}"
  end

  def test_multiple_valid_jobs_with_no_dependencies
    input = "a =>\nb => c\nc =>"
    ordered_jobs = @job_manager.order_jobs(input)

    assert_equal ordered_jobs.chars.sort.join, "abc", "input = #{input}, result = #{ordered_jobs}"
  end

  def test_multiple_valid_jobs_with_one_dependency
    input = "a =>\nb => c\nc =>"
    ordered_jobs = @job_manager.order_jobs(input)

    # B depends on C, so this order must exist
    assert ordered_jobs.include?("cb"), "input = #{input}, result = #{ordered_jobs}"
    # There must be one occurrence of a in the string, order not important
    assert_equal ordered_jobs.count("a"), 1, "input = #{input}, result = #{ordered_jobs}"
  end

  def test_multiple_valid_jobs_and_multiple_dependencies
    input = "a =>\nb => c\nc => f\nd => a\ne => b\nf =>"
    ordered_jobs = @job_manager.order_jobs(input)

    # Assert dependent orders exist
    # F before C
    assert ordered_jobs.index("f") < ordered_jobs.index("c"), "input = #{input}, result = #{ordered_jobs}"
    # C before B
    assert ordered_jobs.index("c") < ordered_jobs.index("b"), "input = #{input}, result = #{ordered_jobs}"
    # B before E
    assert ordered_jobs.index("b") < ordered_jobs.index("e"), "input = #{input}, result = #{ordered_jobs}"
    # A before D
    assert ordered_jobs.index("a") < ordered_jobs.index("d"), "input = #{input}, result = #{ordered_jobs}"
  end

  def test_invalid_state_single_job_with_missing_dependency
    input = "a => b"
    ordered_jobs = @job_manager.order_jobs(input)

    assert_raise "#{ordered_jobs}"
  end

  def test_invalid_state_job_depends_on_itself
    input = "a =>\nb =>\nc => c"
    ordered_jobs = @job_manager.order_jobs(input)

    assert_raise "#{ordered_jobs}"
  end

  def test_invalid_state_jobs_with_circular_dependencies
    input = "a =>\nb => c\nc => f\nd => a\ne =>\nf => b"
    ordered_jobs = @job_manager.order_jobs(input)

    assert_raise "#{ordered_jobs}"
  end
end
